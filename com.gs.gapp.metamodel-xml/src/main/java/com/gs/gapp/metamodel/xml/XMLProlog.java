package com.gs.gapp.metamodel.xml;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;


public class XMLProlog extends ModelElement {

	private static final long serialVersionUID = 7970669439379725845L;

	private final Set<XMLProcessingInstruction> processingInstructions = new LinkedHashSet<>();

	private final Set<XMLDoctype> doctypes = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public XMLProlog(String name) {
		super(name);
	}

	/**
	 * @return the processingInstructions
	 */
	public Set<XMLProcessingInstruction> getProcessingInstructions() {
		return Collections.unmodifiableSet(processingInstructions);
	}

	/**
	 * @param processingInstruction
	 * @return
	 */
	public boolean addProcessingInstruction(XMLProcessingInstruction processingInstruction) {
		return this.processingInstructions.add(processingInstruction);
	}

	/**
	 * @return the doctypes
	 */
	public Set<XMLDoctype> getDoctypes() {
		return Collections.unmodifiableSet(doctypes);
	}

	/**
	 * @param doctype
	 * @return
	 */
	public boolean addDoctype(XMLDoctype doctype) {
		return this.doctypes.add(doctype);
	}
}

package com.gs.gapp.metamodel.xml;



/**
 * @author mmt
 *
 */
public class XMLDocumentationElement extends XMLElement {

	private static final long serialVersionUID = 1418942374408727114L;

	/**
	 * @param name
	 */
	public XMLDocumentationElement(String name) {
		super(name);
	}
	
	
}

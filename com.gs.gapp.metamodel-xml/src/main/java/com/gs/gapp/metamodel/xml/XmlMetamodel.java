package com.gs.gapp.metamodel.xml;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * TODO this is work in progress (mmt 12-Sep-2022)
 *
 */
public enum XmlMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		// TODO complete this list (mmt 12-Sep-2022)
		metatypes.add(XMLDocument.class);
		
		collectionOfCheckedMetatypes.add(XMLDocument.class);
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}

	@Override
	public Set<Class<?>> getMetaTypesForIndirectProcessing() {
		Set<Class<?>> result = new HashSet<>(MetamodelI.super.getMetaTypesForIndirectProcessing());
		return result;
	}
}

package com.gs.gapp.metamodel.xml;



public class XMLPlaceholderElement extends XMLElement {

	private static final long serialVersionUID = -7022878073449675819L;
	
	private String content;

	/**
	 * @param name
	 * @param tag
	 * @param namespace
	 */
	public XMLPlaceholderElement(String name, String tag, XMLNamespace namespace) {
		super(name, tag, namespace);
	}

	/**
	 * @param name
	 * @param tag
	 */
	public XMLPlaceholderElement(String name, String tag) {
		super(name, tag);
	}

	/**
	 * @param name
	 */
	public XMLPlaceholderElement(String name) {
		super(name);
	}
	
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}

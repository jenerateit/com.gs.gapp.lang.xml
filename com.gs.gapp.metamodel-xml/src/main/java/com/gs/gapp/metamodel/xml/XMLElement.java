package com.gs.gapp.metamodel.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElement;


public class XMLElement extends ModelElement {

	private static final long serialVersionUID = 7970669439379725845L;

	private XMLDocument document;
	private XMLAttribute id;
	private String prefixForId;
	private String tag;
	private String value;
	private boolean nillable = true;
	private boolean required = false;

	private XMLNamespace namespace;
	private XMLElement parent;
	private final List<XMLElement> children = new ArrayList<>();
	private final Set<XMLAttribute> attributes = new LinkedHashSet<>();
	
	private String hint;
	private final Set<String> activateableAttributes = new LinkedHashSet<>();
	private final Set<String> attributesIgnoredFromExistingFile = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public XMLElement(String name) {
		this(name, null);
	}
	
	public XMLElement(String name, String tag) {
		this(name, tag, null);
	}
	
	public XMLElement(String name, String tag, XMLNamespace namespace) {
		super(name);
		this.setIdAttribute(getTechnicalName());
		this.tag = tag;
		this.namespace = namespace;
	}
	
	protected void setIdAttribute(String id) {
		if (this.id != null) this.attributes.remove(this.id);
		if (id != null && id.length() > 0) {
			this.id = new XMLAttribute("id", id);
			this.addAttribute(this.id);
		} else {
			this.id = null;
		}
	}

	public String getIdAttribute() {
		return this.id == null ? null : id.getValue();
	}
	
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the nillable
	 */
	public boolean isNillable() {
		return nillable;
	}

	/**
	 * @param nillable the nillable to set
	 */
	public void setNillable(boolean nillable) {
		this.nillable = nillable;
	}

	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	/**
	 * @return the namespace
	 */
	public XMLNamespace getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(XMLNamespace namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return
	 */
	@Override
	public String getQualifiedName() {
		return (getNamespace() == null || StringTools.isEmpty(getNamespace().getName())) ?
				    getTag() : new StringBuilder(getNamespace().getName()).append(":").append(getTag()).toString();
	}
	
	/**
	 * This method creates an id that is unique within the whole XML document.
	 * Amongst others, this qualified id can be used to provide developer area keys.
	 *  
	 * @return
	 */
	public String getQualifiedId() {
		StringBuilder qualifiedId = new StringBuilder(this.getQualifiedName());
		if (getIdAttribute() != null && getIdAttribute().length() > 0) qualifiedId.append("[").append(getIdAttribute()).append("]");
		XMLElement parentElement = getParent();
		while (parentElement != null) {
			qualifiedId.insert(0, "/").insert(0, parentElement.getQualifiedName());
			parentElement = parentElement.getParent();
		}
		
		return qualifiedId.toString().replace(":", "_");
	}
	
	/**
	 * @return the parent
	 */
	public XMLElement getParent() {
		return parent;
	}
	
	/**
	 * @param <T>
	 * @param xmlElementType
	 * 
	 * @return
	 */
	public <T extends XMLElement> XMLElement getParent(Class<T> xmlElementType) {
		XMLElement aParent = this.parent;
		while (aParent != null) {
			if (xmlElementType.isAssignableFrom(aParent.getClass())) {
				return aParent;
			}
			aParent = aParent.getParent();
		}
		
		return null;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(XMLElement parent) {
		this.parent = parent;
	}

	/**
	 * @return the children
	 */
	public List<XMLElement> getChildren() {
		return Collections.unmodifiableList(children);
	}
	
	public <T extends XMLElement> XMLElement getFirstOccurrenceInChildTree(Class<T> xmlElementType) {
		for (XMLElement childElement : this.children) {
			if (xmlElementType.isAssignableFrom(childElement.getClass())) {
				return childElement;
			}
			return childElement.getFirstOccurrenceInChildTree(xmlElementType);
		}
		
		return null;
	}
	
	/**
	 * @param <T>
	 * @param xmlElementType
	 * @return
	 */
	public <T extends XMLElement> Set<T> getChildren(Class<T> xmlElementType) {
		LinkedHashSet<T> collectedChildren = new LinkedHashSet<>();
		collectChildren(xmlElementType, collectedChildren);
		return collectedChildren;
	}
	
	/**
	 * @param xmlElementType
	 * @param collectedElements
	 */
	@SuppressWarnings("unchecked")
	private <T extends XMLElement> void collectChildren(Class<T> xmlElementType, LinkedHashSet<T> collectedElements) {
		if (xmlElementType.isAssignableFrom(this.getClass())) {
			collectedElements.add((T) this);
		}
		if (this.children != null) {
			for (XMLElement childElement : this.children) {
				childElement.collectChildren(xmlElementType, collectedElements);
			}
		}
	}

	/**
	 * @param element
	 * @return
	 */
	public boolean addChild(XMLElement element) {
		if (this.children.contains(element)) return false;
		boolean result = this.children.add(element);
		element.setParent(this);
		if (element.getPrefixForId() == null || element.getPrefixForId().length() == 0) {
			// automatically set this prefix in case there is not yet a prefix set
		    element.setPrefixForId(this.getName());
		}
		return result;
	}
	
	/**
	 * @param element
	 * @return
	 */
	public boolean addBefore(XMLElement beforeThis, XMLElement element) {
		if (this.children.contains(element)) return false;
		int idx = this.children.indexOf(beforeThis);
		if (idx < 0) return false;
		this.children.add(idx, element);
		element.setParent(this);
		if (element.getPrefixForId() == null || element.getPrefixForId().length() == 0) {
			// automatically set this prefix in case there is not yet a prefix set
			element.setPrefixForId(this.getName());
		}
		
		return true;
	}
	
	public boolean addAfter(XMLElement afterThis, XMLElement element) {
		if (this.children.contains(element)) return false;
		int idx = this.children.indexOf(afterThis);
		if (idx < 0) return false;
		this.children.add(idx+1, element);
		element.setParent(this);
		if (element.getPrefixForId() == null || element.getPrefixForId().length() == 0) {
			// automatically set this prefix in case there is not yet a prefix set
			element.setPrefixForId(this.getName());
		}
		
		return true;
	}
	
	public boolean removeChild(XMLElement element) {
		return this.children.remove(element);
	}

	/**
	 * @return the attributes
	 */
	public Set<XMLAttribute> getAttributes() {
		return Collections.unmodifiableSet(attributes);
	}
	
	public XMLAttribute getAttribute(String name) {
		for (XMLAttribute attribute : attributes) {
			if (attribute.getName().equalsIgnoreCase(name)) return attribute;
		}
		
		return null;
	}
	
	public String getAttributeValue(String name) {
		XMLAttribute xmlAttribute = this.getAttribute(name);
		if (xmlAttribute != null) return xmlAttribute.getValue();
		return null;
	}

	/**
	 * @param attribute
	 * @return
	 */
	public boolean addAttribute(XMLAttribute attribute) {
		attribute.setOwner(this);
		return this.attributes.add(attribute);
	}
	
	public XMLAttribute addAttribute(String name, String value) {
		XMLAttribute attribute = getAttribute(name);
		if (attribute != null) {
			attribute.setValue(value);
		} else {
			attribute = new XMLAttribute(name, value);
			this.addAttribute(attribute);
		}
		return attribute;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result
				+ ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	/**
	 * @return
	 */
	public boolean isEmpty() {
		return getChildren().size() == 0 && StringTools.isEmpty(getValue());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		XMLElement other = (XMLElement) obj;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		if (parent == null) {
			if (other.parent!= null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder(super.toString()).append("/").append(namespace == null ? "no-namespace" : namespace.getName())
				.append("/").append(attributes.toString()).toString();
	}

	public String getTag() {
		return this.tag;
	}

	public XMLDocument getDocument() {
		if (document != null) return document;
		
		XMLElement aParent = this.parent;
		while (aParent != null) {
			if (aParent.getDocument() != null) return aParent.getDocument();
			aParent = aParent.getParent();
		}
		
		return null;
	}

	public void setDocument(XMLDocument document) {
		this.document = document;
	}

	public String getPrefixForId() {
		return prefixForId;
	}

	public void setPrefixForId(String prefixForId) {
		this.prefixForId = prefixForId;
	}
	
	public String getBetterQualifiedId() {
		if (this.prefixForId != null && this.prefixForId.length() > 0 && getIdAttribute() != null && getIdAttribute().length() > 0) {
		    return this.prefixForId + "_" + getIdAttribute();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public String getEffectiveId() {
		String betterQualifiedId = getBetterQualifiedId();
		String idAttribute = getIdAttribute();
		
		return betterQualifiedId != null && betterQualifiedId.length() > 0 ?
				betterQualifiedId : idAttribute;
	}
	
	/**
	 * @return
	 */
	public String getCombinedId() {
		String effectiveId = getEffectiveId();
		String effectiveParentId = this.getParent() != null ?
				this.getParent().getEffectiveId() : null;
				
		if (effectiveId == null || effectiveParentId == null) {
			return null;
		}
		
		return new StringBuilder(effectiveId).append("#").append(effectiveParentId).toString(); 
	}

	/**
	 * @return the hint
	 */
	public String getHint() {
		return hint;
	}

	/**
	 * @param hint the hint to set
	 */
	public void setHint(String hint) {
		this.hint = hint;
	}
	
	/**
	 * <p>This method returns an XML element's attribute names that need special handling in the to-be-generated XML file.
	 * 
	 * <p>An attribute whose name is part of the set that is returned by this method is not being generated, unless the attribute is
	 * already present in the generated XML file. With this you initially get fewer attributes generated and can activate
	 * the generation of these attributes by adding [attribute-name]="" to the XML element.
	 * 
	 * <p>This functionality got introduced for XML files that have attribute values that refer to Java code,
	 * e.g. XHTML files where attribute values can contain expressions that serve the purpose of calling Java code.
	 * 
	 * @return
	 */
	public Set<String> getActivateableAttributes() {
		return activateableAttributes;
	}
	
	/**
	 * <p>Add names of attributes to the collection returned by this method
	 * in order to prevent those attributes from being regenerated in case they are part
	 * of the existing XML file.
	 * 
	 * @return
	 */
	public Set<String> getAttributesIgnoredFromExistingFile() {
		return attributesIgnoredFromExistingFile;
	}

	/**
	 * <p>The return value of this method tells the generator that an attribute that already has a value that includes that
	 * value should be generated pertaining the value that initially had been manually entered by a developer. In all other
	 * cases, the generator can dictate the attribute's value.
	 * 
	 * <p>Note that this mechanism only works for attributes with names
	 * that are returned from {@link XMLElement#getActivateableAttributes()}.
	 * 
	 * @param attributeName
	 * @return
	 */
	public String getIndicatorForManuallyAddedAttribute(String attributeName) {
		return null;
	}
	
	/**
	 * <p>When an attribute has a preexisting value in the XML file that does _not_ contain
	 * the string that gets returned by this method, then that value will not be overwritten
	 * by the generator.
	 * 
	 * <p>Note that this mechanism only works for attributes with names
	 * that are returned from {@link XMLElement#getActivateableAttributes()}.
	 * 
	 * @param attributeName
	 * @return
	 */
	public String getNegativeIndicatorForManuallyAddedAttribute(String attributeName) {
		return null;
	}
}

package com.gs.gapp.metamodel.xml;

import java.util.Objects;

import com.gs.gapp.metamodel.basic.BasicMetaModelUtil;
import com.gs.gapp.metamodel.basic.ModelElement;


public class XMLDocument extends ModelElement {

	private static final long serialVersionUID = 7970669439379725845L;

	private XMLProlog prolog;

	private XMLElement rootElement;
	
	private String filenamePostfix;
	
	private final String path;

	/**
	 * @param name
	 */
	public XMLDocument(String name) {
		this(name, null);
	}
	
	public XMLDocument(String name, String path) {
		super(BasicMetaModelUtil.normalizeFileNameNoSpaces(name));
		this.path = BasicMetaModelUtil.normalizePathNoSpaces(path, "");
	}

	/**
	 * @return the prolog
	 */
	public XMLProlog getProlog() {
		return prolog;
	}

	/**
	 * @param prolog the prolog to set
	 */
	public void setProlog(XMLProlog prolog) {
		this.prolog = prolog;
	}

	/**
	 * @return the rootElement
	 */
	public XMLElement getRootElement() {
		return rootElement;
	}

	/**
	 * @param rootElement the rootElement to set
	 */
	public void setRootElement(XMLElement rootElement) {
		this.rootElement = rootElement;
		if (rootElement != null) rootElement.setDocument(this);
	}

	public String getFilenamePostfix() {
		return filenamePostfix;
	}

	public void setFilenamePostfix(String filenamePostfix) {
		this.filenamePostfix = filenamePostfix;
	}

	/**
	 * A relative path that is going to be prepended to the file name.
	 * It is ignored when it is null or "".
	 * Example:
	 * - target dir = src/main/resource
	 * - path = /META-INF
	 * - name = persistence.xml
	 * ... results in complete file name: src/main/resource/META-INF/persistence.xml
	 * 
	 * Note that path is used in equals and hashcode in case it is not null
	 * and not an empty string.
	 * 
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#hashCode()
	 */
	@Override
	public int hashCode() {
		String originatingName = null;
		if (getOriginatingElement(ModelElement.class) != null) {
			originatingName = getOriginatingElement(ModelElement.class).getName();
		}
		
        if ((path == null || path.length() == 0) && originatingName != null) {
			return Objects.hash(super.hashCode(), originatingName);
        } else if ((path != null && path.length() > 0) && originatingName == null) {
        	return Objects.hash(super.hashCode(), path);
		} else if ((path != null && path.length() > 0) && originatingName != null) {
			return Objects.hash(super.hashCode(), path, originatingName);
		}
		
		return super.hashCode();
	}
}

package com.gs.gapp.metamodel.xml;

import com.gs.gapp.metamodel.basic.ModelElement;


public class XMLNamespace extends ModelElement {

	private static final long serialVersionUID = 7970669439379725845L;

	private final String uri;

	/**
	 * @param name
	 * @param uri
	 */
	public XMLNamespace(String name, String uri) {
		super(name);
		this.uri = uri;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}
}

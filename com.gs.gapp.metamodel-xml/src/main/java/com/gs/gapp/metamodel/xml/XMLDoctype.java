package com.gs.gapp.metamodel.xml;

import com.gs.gapp.metamodel.basic.ModelElement;

public class XMLDoctype extends ModelElement {

	private static final long serialVersionUID = 2723021114132974396L;

	private final String doctype;

	public XMLDoctype(String name, String doctype) {
		super(name);
		this.doctype = doctype;
	}

	/**
	 * @return the doctype
	 */
	public String getDoctype() {
		return doctype;
	}

}


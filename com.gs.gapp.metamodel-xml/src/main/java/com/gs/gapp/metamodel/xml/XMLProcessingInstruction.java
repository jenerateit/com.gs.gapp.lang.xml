package com.gs.gapp.metamodel.xml;

import com.gs.gapp.metamodel.basic.ModelElement;


public class XMLProcessingInstruction extends ModelElement {

	private static final long serialVersionUID = 2723021114132974396L;

	private final String processingInstruction;

	public XMLProcessingInstruction(String name, String processingInstruction) {
		super(name);
		this.processingInstruction = processingInstruction;
	}

	/**
	 * @return the processingInstruction
	 */
	public String getProcessingInstruction() {
		return processingInstruction;
	}
}

package com.gs.gapp.metamodel.xml;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElement;


public class XMLAttribute extends ModelElement {

	private static final long serialVersionUID = 7970669439379725845L;

	private XMLNamespace namespace;
	private String value;
	private boolean required = false;
	private XMLElement owner;

	
	public XMLAttribute(String name, String value) {
		super(name);
		this.value = value;
	}

	/**
	 * @return the namespace
	 */
	public XMLNamespace getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(XMLNamespace namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	@Override
	public String getQualifiedName() {
		return (getNamespace() == null || StringTools.isEmpty(getNamespace().getName())) ?
				    getName() : new StringBuilder(getNamespace().getName()).append(":").append(getName()).toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		XMLAttribute other = (XMLAttribute) obj;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder(super.toString()).append("/").append(namespace == null ? "no-namespace" : namespace.getName())
				.append("/").append(value).toString();
	}

	public XMLElement getOwner() {
		return owner;
	}

	public void setOwner(XMLElement owner) {
		this.owner = owner;
	}
}

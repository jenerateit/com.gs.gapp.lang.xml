/*
 * Copyright (c) 2008 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.generation.xml.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;

import com.gs.gapp.generation.basic.TargetModelizer;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.xml.XMLDocument;

/**
 * {@link TargetI} implementation for a XML target.
 *
 * @author hrr
 */
public abstract class XmlTarget extends BasicTextTarget<XmlTargetDocument> {
	
	private static final TargetLifecycleListenerI TARGET_MODELIZER = new TargetModelizer();
	
	@ModelElement
	private XMLDocument xmlDocument;

	/**
	 * Constructor
	 */
	protected XmlTarget() {
		super();
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
		    addTargetLifecycleListener(TARGET_MODELIZER);
		}
	}
	
	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder(xmlDocument.getName());
		try {
			return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
}

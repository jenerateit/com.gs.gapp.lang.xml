/**
 *
 */
package com.gs.gapp.generation.xml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.xml.target.XMLDocumentTarget;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionLong;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;


/**
 * @author mmt
 *
 */
public class GenerationGroupXML extends AbstractGenerationGroup implements GenerationGroupConfigI {

	private final WriterLocatorI writerLocator;
	
	/**
	 *
	 */
	public GenerationGroupXML() {
		super(new ModelElementCache());
		addTargetClass(XMLDocumentTarget.class);
		writerLocator = new WriterLocatorXML(getAllTargets());
	}

	/**
	 * @return the writerLocator
	 */
	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class GenerationGroupXmlOptions extends GenerationGroupOptions {
		
		public static final OptionDefinitionLong OPTION_DEF_FORMAT_ATTRIBUTES_PER_LINE =
				new OptionDefinitionLong("format.number-of-attributes-per-line",
						                 "defines how many attributes are put on one single line", false, 3L);
		
		public static final OptionDefinitionLong OPTION_DEF_FORMAT_LINE_WIDTH =
				new OptionDefinitionLong("format.line-width",
						                 "defines the maximum line widht, note that actual line widths could be longer when it is not possible to add a newline in between", false, 3L);
		
		public static final OptionDefinitionBoolean OPTION_DEF_XML_SORT_ATTRIBUTES =
				new OptionDefinitionBoolean("xml.sort-attributes",
						                    "set this to true to sort attributes of an XML element alphabetically", false, false);
		
		public static final OptionDefinitionBoolean OPTION_DEF_XML_GENERATE_TAG_COMMENTS =
				new OptionDefinitionBoolean("xml.generate-tag-comments",
						                    "Set this to true to generate XML comments like this: <!-- ... -->, containing the comment from the model.", false, Boolean.FALSE);
		
		public static final OptionDefinitionString OPTION_DEF_XML_ATTRIBUTE_PRESERVATION_PATTERN =
				new OptionDefinitionString("xml.attribute-preservation-pattern",
						                    "A pattern of the form '[attribute-name]=[value-pattern]", false, null);
		
		private final Map<String,String> attributePreservationPatterns = new HashMap<>();
		
		public GenerationGroupXmlOptions(AbstractWriter writer) {
			super(writer);
		}
		
		public GenerationGroupXmlOptions(TargetI<?> target) {
			super(target);
		}

		/**
		 * @return
		 */
		public long getNumberOfAttributesPerLine() {
			long numberOfAttributesPerLine = 3;
			
			Serializable optionValueAsString = getOptionValue(OptionDefinitionEnum.OPTION_FORMAT_ATTRIBUTES_PER_LINE.getName());
			validateNumericOption(optionValueAsString, OptionDefinitionEnum.OPTION_FORMAT_ATTRIBUTES_PER_LINE.getName());
			
			if (optionValueAsString != null) {
				try {
			        numberOfAttributesPerLine = Long.parseLong(optionValueAsString.toString());
				} catch (IllegalArgumentException ex) {
                    throw new WriterException("incorrect option value '" + optionValueAsString +"' for option " + OPTION_DEF_FORMAT_ATTRIBUTES_PER_LINE.getKey());
				}
			}
			
			return numberOfAttributesPerLine;
		}
		
		/**
		 * TODO this option is not yet used since at the time being the VD-API does not allow to find out how long the currently written line would get. Use it once this will become possible. (mmt 26-Sep-2018)
		 * 
		 * @return
		 */
		public long getLineWidth() {
			long lineWidth = 72;
			
			Serializable optionValueAsString = getOptionValue(OptionDefinitionEnum.OPTION_FORMAT_LINE_WIDTH.getName());
			validateNumericOption(optionValueAsString, OptionDefinitionEnum.OPTION_FORMAT_LINE_WIDTH.getName());
			
			if (optionValueAsString != null) {
				try {
			        lineWidth = Long.parseLong(optionValueAsString.toString());
				} catch (IllegalArgumentException ex) {
                    throw new WriterException("incorrect option value '" + optionValueAsString +"' for option " + OPTION_DEF_FORMAT_LINE_WIDTH.getKey());
				}
			}
			
			return lineWidth;
		}
		
		/**
		 * @return
		 */
		public boolean sortAttributes() {
			Serializable optionValueAsString = getOptionValue(OptionDefinitionEnum.OPTION_XML_SORT_ATTRIBUTES.getName());
			validateBooleanOption(optionValueAsString, OptionDefinitionEnum.OPTION_XML_SORT_ATTRIBUTES.getName());
			if (optionValueAsString != null) {
				return Boolean.parseBoolean(optionValueAsString.toString());
			}
			
			return false;
		}
		
		/**
		 * @return
		 */
		public boolean generateTagComments() {
			Serializable optionValueAsString = getOptionValue(OptionDefinitionEnum.OPTION_XML_GENERATE_TAG_COMMENTS.getName());
			validateBooleanOption(optionValueAsString, OptionDefinitionEnum.OPTION_XML_GENERATE_TAG_COMMENTS.getName());
			if (optionValueAsString != null) {
				return Boolean.parseBoolean(optionValueAsString.toString());
			}
			
			return false;
		}
		
		/* (non-Javadoc)
		 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getOptionDefinitions()
		 */
		@Override
		public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
			Set<OptionDefinition<? extends Serializable>> result = super.getOptionDefinitions();
			result.addAll(OptionDefinitionEnum.getDefinitions());
			return result;
		}
		
		/**
		 * @return
		 */
		public String getAttributePreservationPattern() {
			Serializable optionValueAsString = getOptionValue(OptionDefinitionEnum.OPTION_XML_ATTRIBUTE_PRESERVATION_PATTERN.getName());
			return optionValueAsString == null ? null : optionValueAsString.toString().trim();
		}
		
		/**
		 * @return the attributePreservationPattern
		 */
		public Map<String,String> getAttributePreservationPatterns() {
			if (getAttributePreservationPattern() != null && getAttributePreservationPattern().length() > 0) {
				if (attributePreservationPatterns.isEmpty()) {
			    	String[] patternArray = getAttributePreservationPattern().split(",");
			    	for (String pattern : patternArray) {
			    		if (pattern != null && pattern.length() > 0) {
			    			String[] keyValuePair = pattern.split("=");
			    			if (keyValuePair.length == 2 && keyValuePair[0] != null) {
			    				attributePreservationPatterns.put(keyValuePair[0].trim(), keyValuePair[1]);
			    			}
			    		}
			    	}
				}
			}
			
			return attributePreservationPatterns;
		}
		
		/**
		 * @author mmt
		 *
		 */
		public enum OptionDefinitionEnum {

			
			OPTION_FORMAT_ATTRIBUTES_PER_LINE ( OPTION_DEF_FORMAT_ATTRIBUTES_PER_LINE ),
			OPTION_FORMAT_LINE_WIDTH ( OPTION_DEF_FORMAT_LINE_WIDTH ),
			OPTION_XML_SORT_ATTRIBUTES (OPTION_DEF_XML_SORT_ATTRIBUTES),
			OPTION_XML_GENERATE_TAG_COMMENTS (OPTION_DEF_XML_GENERATE_TAG_COMMENTS),
			OPTION_XML_ATTRIBUTE_PRESERVATION_PATTERN (OPTION_DEF_XML_ATTRIBUTE_PRESERVATION_PATTERN),
			;

			private static final Map<String, OptionDefinitionEnum> stringToEnum = new HashMap<>();

			static {
				for (OptionDefinitionEnum m : values()) {
					stringToEnum.put(m.getName(), m);
				}
			}
			
			/**
			 * @return
			 */
			public static Set<OptionDefinition<? extends Serializable>> getDefinitions() {
				Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<>();
				for (OptionDefinitionEnum m : values()) {
					result.add(m.getDefinition());
				}
				return result;
			}

			/**
			 * @param datatypeName
			 * @return
			 */
			public static OptionDefinitionEnum fromString(String datatypeName) {
				return stringToEnum.get(datatypeName);
			}

			private final OptionDefinition<? extends Serializable> definition;
			
			private OptionDefinitionEnum(OptionDefinition<? extends Serializable> definition) {
				this.definition = definition;
			}

			/**
			 * @return the name
			 */
			public String getName() {
				return this.getDefinition().getName();
			}

			public OptionDefinition<? extends Serializable> getDefinition() {
				return definition;
			}
		}
	}
}

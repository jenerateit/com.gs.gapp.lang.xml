package com.gs.gapp.generation.xml.writer;

import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.gs.gapp.generation.basic.GenerationPreventableByUserI;
import com.gs.gapp.generation.xml.target.XMLDocumentTarget;
import com.gs.gapp.generation.xml.target.XMLDocumentTargetDocument;
import com.gs.gapp.metamodel.basic.GeneratorInfo;
import com.gs.gapp.metamodel.xml.XMLDocument;

public class XMLDocumentWriter extends XmlWriter {

	@ModelElement
	private XMLDocument document;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		
		checkForGenerationPrevention();

		if (XMLDocumentTargetDocument.PROLOG == ts) {
            wProlog(ts);
		} else if (XMLDocumentTargetDocument.CONTENT == ts) {
			wDocumentDescription(ts);
            wContent(ts);
		}
	}
	
	/**
	 * @return
	 */
	protected boolean isManuallyAddedAttributePreserved() {
		return false;
	}
	
	protected void checkForGenerationPrevention() {
		
		if (getTransformationTarget().getPreviousTargetDocument() instanceof GenerationPreventableByUserI) {
		
			boolean isAdditionallyGeneratedFile = getTransformationTarget().getTargetPath().toString().indexOf("_generated.") > 0;
			TargetDocumentI previousTargetDocument = getTransformationTarget().getPreviousTargetDocument();
			if (previousTargetDocument != null) {
				GenerationPreventableByUserI preventableByUser = (GenerationPreventableByUserI) previousTargetDocument;
				boolean generationPreventedByUser = preventableByUser.isGenerationPreventedByUser();
				
				
				if (isAdditionallyGeneratedFile) {
					// instead of the following, commented complex logic, that does not work properly, we are simply always generating the _generated file
	//				boolean generationOfOriginalFilePreventedByUser = false;
	//				@SuppressWarnings("unchecked")
	//				SortedSet<TargetI<?>> xmlTargets = findTargets(FindTargetScope.PROJECT, document, (Class<? extends TargetI<?>>) getTransformationTarget().getClass(), null);
	//				for (TargetI<? extends TargetDocumentI> target : xmlTargets) {
	//					XMLDocumentTarget xmlDocumentTarget = (XMLDocumentTarget) target;
	//					if (getTransformationTarget().getClass() == xmlDocumentTarget.getClass() && !getTransformationTarget().getTargetPath().equals(xmlDocumentTarget.getTargetPath())) {
	//						preventableByUser = (GenerationPreventableByUserI) target.getPreviousTargetDocument();
	//						if (preventableByUser != null) generationOfOriginalFilePreventedByUser = preventableByUser.isGenerationPreventedByUser();			
	//					}
	//				}
	//				// ... means the file name includes the string "_generated"
	//				if (!generationOfOriginalFilePreventedByUser) throw new DoNotWriteException("user has not prevented the generation of the original target file that is related to  the file '" + getTransformationTarget().getTargetPath() + "' => we do not need to generate the file with the name that includes '_generated'");
				} else {
					if (generationPreventedByUser) {
						throw new DoNotWriteException("user has prevented the generation of the target file '" + getTransformationTarget().getTargetPath() + "' by adding the string 'DO_NOT_GENERATE' in the generated file => we do not overwrite this file");
					} else {
//						System.out.println("Target " + getTransformationTarget().getTargetPath() + ", state isNew:" + getTransformationTarget().getTargetInfo().getStatus().isNew() + ", state isRead:" + getTransformationTarget().getTargetInfo().getStatus().isReaded());
    					// TODO this is a temporary hack in order to avoid that a user looses manual modifications (see VIDE-215)
//						throw new DoNotWriteException("temporary hack in order to avoid that a user looses manual modifications in target:'" + getTransformationTarget().getTargetPath() + "'");
					}
				}
			} else {
				// --- TODO no previous target document, what to be done here? Is this state possible?
//				throw new DoNotWriteException("previous document is null, we are not writing anything:'" + getTransformationTarget().getTargetPath() + "'");
//				System.out.println("Target " + getTransformationTarget().getTargetPath() + ", state isNew:" + getTransformationTarget().getTargetInfo().getStatus().isNew() + ", state isRead:" + getTransformationTarget().getTargetInfo().getStatus().isReaded());
			}
		}
	}
	
	/**
	 * @param ts
	 */
	protected void wDocumentDescription(TargetSection ts) {
		StringBuilder documentDescription = new StringBuilder();
		if (document.getBody() != null && document.getBody().length() > 0) {
			documentDescription.append(document.getBody());
		}
		
		GeneratorInfo generatorInfo = this.document.getModel().getLeadingGeneratorInfo();
		if (generatorInfo != null) {
			StringBuilder generatorInfoFormatted = GeneratorInfo.formatGeneratorInfo(document);
			if (documentDescription.length() > 0) {
				documentDescription.append(System.lineSeparator());
			}
			documentDescription.append(generatorInfoFormatted);
		}
		
		if (documentDescription.length() > 0) {
			wComment(documentDescription.toString());
		}
		
		// --- AXML-8
		String linkToOriginalModelElement = this.document.getLinkToOriginalModelElement();
		if (linkToOriginalModelElement != null && !linkToOriginalModelElement.isEmpty()) {
			wComment( new StringBuilder("link to model element: ").append(linkToOriginalModelElement).toString());
		}
	}

	/**
	 * @param ts
	 */
	protected void wContent(TargetSection ts) {
		if (document.getRootElement() != null) {
			if (isManuallyAddedAttributePreserved()) {
				// Getting the previous document makes sure that the previous document is parsed and its elements' attributes are stored in the target document object.
				XMLDocumentTarget.class.cast(getTextTransformationTarget()).getPreviousTargetDocument().getPreviousDocument();
			}
			WriterI rootElementWriter = getTransformationTarget().getWriterInstance(document.getRootElement());
			if (rootElementWriter != null) rootElementWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wProlog(TargetSection ts) {
		if (document.getProlog() != null) {
			WriterI prologWriter = getTransformationTarget().getWriterInstance(document.getProlog());
	    	if (prologWriter != null) prologWriter.transform(ts);
		}
	}
	
	public Map<String,String> getKeyValueMap(String xPathExpression, String keyTagName, String valueTagName) {
		Document previousXmlDocument = XMLDocumentTarget.class.cast(getTextTransformationTarget()).getPreviousTargetDocument().getPreviousDocument();
		
		Map<String,String> keyValueMap = new HashMap<>();
		
		try {
			if (previousXmlDocument != null) {
				XPathFactory xPathfactory = XPathFactory.newInstance();
				XPath xpath = xPathfactory.newXPath();
				XPathExpression expr;
				try {
					expr = xpath.compile(xPathExpression);
					NodeList contextParamElements = (NodeList) expr.evaluate(previousXmlDocument, XPathConstants.NODESET);
					for (int ii=0; ii<contextParamElements.getLength(); ii++) {
						Node node = contextParamElements.item(ii);
						String key = null;
						String value = null;
						int countOfElementNodes = 0;
						for (int jj=0; jj<node.getChildNodes().getLength(); jj++) {
							Node childNode = node.getChildNodes().item(jj);
							if (childNode.getNodeType() == Node.ELEMENT_NODE) countOfElementNodes++;
							if (childNode.getFirstChild() == null) continue;
							
							if (keyTagName.equalsIgnoreCase(childNode.getNodeName())) {
								key = childNode.getFirstChild().getNodeValue();
							} else if (valueTagName.equalsIgnoreCase(childNode.getNodeName())) {
								value = childNode.getFirstChild().getNodeValue();
							}
						}
						
						if (key != null && value != null) {
							keyValueMap.put(key, value);
						} else if (key == null && value != null && countOfElementNodes <= 1) {
							keyValueMap.put(null, value);
						}
					}
				} catch (XPathExpressionException e) {
					e.printStackTrace();
					this.getTransformationTarget().addError("A problem has occurred while applying the XPath expression '" + xPathExpression + "' to the XML document '" + previousXmlDocument.getDocumentURI() + "'.", e);
				}
			}
		} catch (Throwable th) {
			th.printStackTrace();
			this.getTransformationTarget().addError("A problem has occurred while applying the XPath expression '" + xPathExpression + "' to the XML document '" + previousXmlDocument.getDocumentURI() + "'.", th);
		}
		
		return keyValueMap;
	}
	
	public String getSingleEntry(String xPathExpression) {
		Document previousXmlDocument = XMLDocumentTarget.class.cast(getTextTransformationTarget()).getPreviousTargetDocument().getPreviousDocument();
		try {
			
			if (previousXmlDocument != null) {
				XPathFactory xPathfactory = XPathFactory.newInstance();
				XPath xpath = xPathfactory.newXPath();
				XPathExpression expr;
				try {
					expr = xpath.compile(xPathExpression);
					Node node = (Node) expr.evaluate(previousXmlDocument, XPathConstants.NODE);
					if (node != null && node.getChildNodes().getLength() > 0) {
						return node.getChildNodes().item(0).getNodeValue();
					}
				} catch (XPathExpressionException ex) {
					ex.printStackTrace();
					this.getTransformationTarget().addError("A problem has occurred while applying the XPath expression '" + xPathExpression + "' to the XML document '" + previousXmlDocument.getDocumentURI() + "'.", ex);
				}
			}
		} catch (Throwable th) {
			th.printStackTrace();
			this.getTransformationTarget().addError("A problem has occurred while applying the XPath expression '" + xPathExpression + "' to the XML document '" + previousXmlDocument.getDocumentURI() + "'.", th);
		}
		
		return null;
	}
}


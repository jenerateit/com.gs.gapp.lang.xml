package com.gs.gapp.generation.xml.writer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterI;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import com.gs.gapp.generation.xml.target.XMLDocumentTarget;
import com.gs.gapp.generation.xml.target.XMLDocumentTargetDocument;
import com.gs.gapp.metamodel.xml.XMLAttribute;
import com.gs.gapp.metamodel.xml.XMLElement;

public class XMLElementWriter extends XmlWriter {

	@ModelElement
	private XMLElement element;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {

		wBeginDeveloperArea(ts);
		writeElement(ts);
		wEndDeveloperArea(ts);
	}
	
	/**
	 * @param ts
	 */
	protected void writeElement(TargetSection ts) {
		
		if (element.getParent() != null) wNL();
		
		XMLDocumentTargetDocument previousDocument =
				(XMLDocumentTargetDocument) this.getTextTransformationTarget().getPreviousTargetDocument();
		List<Element> manuallyAddedNestedElements = null;
		
		if (element.getCombinedId() != null) {
			manuallyAddedNestedElements = previousDocument.getManuallAddedNestedElements().get(element.getCombinedId());
		    List<Element> predecessorElements = previousDocument.getManuallAddedPredecessors().get(element.getCombinedId());
		    if (predecessorElements != null && !predecessorElements.isEmpty()) {
		    	boolean written = false;
		    	for (Element predecessor : predecessorElements) {
		    		if (written) wNL();
		    		wElement(predecessor);
		    		written = true;
		    	}
		    	wNL();
		    }
		}

		if (getGenerationGroupOptions().generateTagComments()) {
			// --- Generate comment/documentation for an XML element, if there is any.
			if (StringTools.isNotEmpty(element.getHint())) {
				w("<!-- ", element.getHint().replace("<", "").replace(">", "").replace("--", "").replace("&", ""), " -->");
				wNL();
			}
		}
		
		final Set<String> activateableAttributes = element.getActivateableAttributes();
		w("<", element.getQualifiedName());
		int numberOfManuallyAddedAttributes = wAttributes(ts);
		if (!element.isEmpty() || manuallyAddedNestedElements != null && !manuallyAddedNestedElements.isEmpty()) {
			w(">");
			if (numberOfManuallyAddedAttributes > 0) {
				w("<!-- manually added attributes:", ""+numberOfManuallyAddedAttributes, " -->");
			}
			if (!activateableAttributes.isEmpty()) {
				w("<!-- activateable:", activateableAttributes.stream().collect(Collectors.joining(", ")), " -->");
			}
		    wValue(ts);
		    wManuallyAddedNestedElements(ts, manuallyAddedNestedElements);
	        wChildElements(ts);
		    w("</", element.getQualifiedName(), ">");
		} else {
			w("/>");
			if (numberOfManuallyAddedAttributes > 0) {
				w("<!-- manually added attributes:", ""+numberOfManuallyAddedAttributes, " -->");
			}
			if (!activateableAttributes.isEmpty()) {
				w("<!-- activateable:", activateableAttributes.stream().collect(Collectors.joining(", ")), " -->");
			}
		}
		
		if (element.getCombinedId() != null) {
		    List<Element> successorElements = previousDocument.getManuallAddedSuccessors().get(element.getCombinedId());
		    if (successorElements != null && !successorElements.isEmpty()) {
		    	for (Element successor : successorElements) {
		    		wNL();
		    		wElement(successor);
		    	}
		    }
		}
	}
	
	private void wManuallyAddedNestedElements(TargetSection ts, List<Element> manuallyAddedNestedElements) {
		if (manuallyAddedNestedElements != null && !manuallyAddedNestedElements.isEmpty()) {
			indent();
				manuallyAddedNestedElements.stream().forEach(element -> {
					wNL();
					wElement(element);
				});
			outdent();
			wNL();
		}
	}

	/**
	 * @param element
	 */
	private final void wElement(Element element) {
		int numberOfChildren = element.getChildNodes().getLength();
		
		w("<", element.getTagName());
		int numberOfAttributes = element.getAttributes().getLength();
		if (numberOfAttributes > 0) {
			indent();
			for (int ii=0; ii<numberOfAttributes; ii++) {
				Attr attribute = (Attr) element.getAttributes().item(ii);
				wNL();
				w(attribute.getName(), "=\"", attribute.getValue(), "\"");
			}
			outdent();
		}
		
		if (numberOfChildren > 0) {
			w(">");
			for (int ii=0; ii<numberOfChildren; ii++) {
				Node childNode = element.getChildNodes().item(ii);
				if (childNode.getNodeType() == Node.ELEMENT_NODE) {
					Element childElement = (Element) childNode;
					wNLI();
					wElement(childElement);
					outdent();
				} else if (childNode.getNodeType() == Node.TEXT_NODE) {
					Text text = (Text) childNode;
					if (text.getData() != null && text.getData().trim().length() > 0) {
						wNLI();
						w(text.getData().trim());
						outdent();
    				}
				}
			}
			wNL();
			w("</", element.getTagName(), ">");
		} else {
			w("/>");
		}
	}

	/**
	 * @param ts
	 */
	private final void wEndDeveloperArea(TargetSection ts) {
		if (getDocumentTarget().getElementWithStartedDeveloperArea() != null && getDocumentTarget().getElementWithStartedDeveloperArea() == element) {
            eDA();
            getDocumentTarget().setElementWithStartedDeveloperArea(null);
		}
	}
	
	/**
	 * @param ts
	 */
	private final void wBeginDeveloperArea(TargetSection ts) {
		if (getDocumentTarget().getElementWithStartedDeveloperArea() == null) {
		    boolean developerAreaStarted = onBeginDeveloperArea(ts);
		    if (developerAreaStarted) {
		    	getDocumentTarget().setElementWithStartedDeveloperArea(element);
		    }
		}
	}
	
	/**
	 * @param ts
	 * @return
	 */
	protected boolean onBeginDeveloperArea(TargetSection ts) {
		return false;
	}
	
	/**
	 * @return
	 */
	protected XMLDocumentTarget getDocumentTarget() {
		return (XMLDocumentTarget) getTransformationTarget();
	}

	protected void wChildElements(TargetSection ts) {
		if (element.getChildren().size() > 0) {
			indent();
			for (XMLElement childElement : element.getChildren()) {
				WriterI elementWriter = getTransformationTarget().getWriterInstance(childElement);
				elementWriter.transform(ts);
			}
			outdent();
			wNL();
		}
	}

	protected void wValue(TargetSection ts) {
		if (StringTools.isNotEmpty(element.getValue())) {
		    w(element.getValue());
		    if (element.getValue().length() > 80) {
		    	wNL();  // in order to move the closing tag into an editor's visible area on a typical computer screen
		    }
		}
	}

	/**
	 * @param ts
	 * @return number of manually added attributes that are written to the target file
	 */
	protected int wAttributes(TargetSection ts) {
		if (element.getAttributes().size() > 0)	w(" ");

		String space = "";
		int counter = 0;
		long numberOfAttributesPerLine = getGenerationGroupOptions().getNumberOfAttributesPerLine();
		ArrayList<XMLAttribute> attributes = new ArrayList<>(element.getAttributes());
		
		// AXML-2
		if (getGenerationGroupOptions().sortAttributes()) {
			Collections.sort(attributes, new Comparator<XMLAttribute>() {

				@Override
				public int compare(XMLAttribute o1, XMLAttribute o2) {
					if (o1 == o2) {
						return 0;
					} else if (o1 == null && o2 != null) {
						return -1;
					} else if (o1 != null && o2 == null) {
						return 1;
					}
					
					return o1.getQualifiedName().compareTo(o2.getQualifiedName());
				}
			});
		}
		
		Set<String> namesOfAllToBeGeneratedAttributes = attributes.stream()
		    .map(xmlAttr -> xmlAttr.getName())
	        .collect(Collectors.toSet());
		XMLDocumentTargetDocument previousDocument =
				(XMLDocumentTargetDocument) this.getTextTransformationTarget().getPreviousTargetDocument();
		final Map<String,XMLAttribute> mapOfPreviousAttributes = new HashMap<>();
		final Set<String> namesOfAllPreviousAttributes = new HashSet<>();
		if (previousDocument != null) {
			previousDocument.getAttributes(element, Collections.emptyList())
		         .stream()
		         .filter(xmlAttribute -> !element.getAttributesIgnoredFromExistingFile().contains(xmlAttribute.getName()))
		         .forEach(xmlAttribute -> mapOfPreviousAttributes.put(xmlAttribute.getName(), xmlAttribute));
		    namesOfAllPreviousAttributes.addAll(mapOfPreviousAttributes.keySet());
		}
		
		// --- generated attributes
		final Set<XMLAttribute> attributesHavingManuallyIndicator = new LinkedHashSet<>();
		final Set<String> namesOfAttributesHavingManuallyIndicator = new HashSet<>();
		for (XMLAttribute attribute : attributes) {
			if (!attribute.isGenerated()) continue;
			
			if (element.getActivateableAttributes().contains(attribute.getName())) {
				if (!namesOfAllPreviousAttributes.contains(attribute.getName())) {
				    // Ignore the generation of an activateable attribute, unless its generation has been activated by it having been manually added to the existing file.
				    continue;
				} else {
					String existingValue = mapOfPreviousAttributes.get(attribute.getName()).getValue();
					if (existingValue == null || existingValue.trim().isEmpty())  {
						// An XML attribute is there, but it doesn't have a value set so far. => generate the attribute content
					} else if (existingValue != null &&
							element.getIndicatorForManuallyAddedAttribute(attribute.getName()) != null &&
							existingValue.contains(element.getIndicatorForManuallyAddedAttribute(attribute.getName()))) {
						// Found the marker that indicates a manually added attribute value => keep that value.
						attributesHavingManuallyIndicator.add(attribute);
						namesOfAttributesHavingManuallyIndicator.add(attribute.getName());
						attribute.setValue(existingValue);
						continue;
					} else if (existingValue != null &&
							element.getNegativeIndicatorForManuallyAddedAttribute(attribute.getName()) != null &&
							!existingValue.contains(element.getNegativeIndicatorForManuallyAddedAttribute(attribute.getName()))) {
						// Found the marker that indicates _not_ a manually added attribute value => keep that value.
						attributesHavingManuallyIndicator.add(attribute);
						namesOfAttributesHavingManuallyIndicator.add(attribute.getName());
						attribute.setValue(existingValue);
						continue;
					} else {
						// An activateable attribute has been found and its value is not empty and it is not marked as being a manually added value. => generate it
					}
				}
			} else if (namesOfAllPreviousAttributes.contains(attribute.getName()) && mapOfPreviousAttributes.get(attribute.getName()).getValue() != null) {
				String existingValue = mapOfPreviousAttributes.get(attribute.getName()).getValue();
				if (existingValue != null && !existingValue.isEmpty() &&
						element.getNegativeIndicatorForManuallyAddedAttribute(attribute.getName()) != null &&
						!existingValue.contains(element.getNegativeIndicatorForManuallyAddedAttribute(attribute.getName()))) {
					// Found the marker that indicates _not_ a manually added attribute value => keep that value.
					attributesHavingManuallyIndicator.add(attribute);
					namesOfAttributesHavingManuallyIndicator.add(attribute.getName());
					attribute.setValue(existingValue);
					continue;
				}
			}
			
			WriterI attributeWriter = getTransformationTarget().getWriterInstance(attribute);

			if (counter == 0) {
				wNLI();
			}
			
			if (counter > 0 && counter % numberOfAttributesPerLine == 0) {
				wNL();
			} else {
			    w(space);
			}
			
			attributeWriter.transform(ts);
			space = " ";
			counter++;
		}

		// --- manually added attributes
		int numberOfManuallyAddedAttributes = 0;
		String effectiveId = element.getEffectiveId();
		if (effectiveId != null && effectiveId.length() > 0) {
			if (previousDocument != null) {
				List<XMLAttribute> existingAttributesNotInConversionResult = previousDocument.getAttributes(element, namesOfAllToBeGeneratedAttributes);
				
				if (!element.getAttributesIgnoredFromExistingFile().isEmpty()) {
					// Warn user about attributes that are in the existing file but are not generated anymore (typically not supported by the XML element anymore).
					List<XMLAttribute> notRegeneratedExistingAttributes = existingAttributesNotInConversionResult.stream()
					    .filter(xmlAttribute -> element.getAttributesIgnoredFromExistingFile().contains(xmlAttribute.getName()))
					    .collect(Collectors.toList());
					if (!notRegeneratedExistingAttributes.isEmpty()) {
						String namesOfAttributes = notRegeneratedExistingAttributes.stream().map(attribute -> attribute.getName() + "=\"" + attribute.getValue() + "\"").collect(Collectors.joining(", "));
						getTextTransformationTarget().addWarning("XML element '" + this.element.getQualifiedName() + "' had attributes in previously existing file that are not preserved: " + namesOfAttributes +
								". Typical reason for this is that these are attributes that the XML element doesn't support anymore.");
					}
				}
				
				final List<XMLAttribute> manuallyAddedAttributes =
						existingAttributesNotInConversionResult.stream()
						    .filter(xmlAttribute -> !element.getAttributesIgnoredFromExistingFile().contains(xmlAttribute.getName()))
						    .collect(Collectors.toList());
				manuallyAddedAttributes.addAll(0, attributesHavingManuallyIndicator);
				if (!manuallyAddedAttributes.isEmpty()) {
					wNL();  // separate manually added attributes by means of an empty line
					for (XMLAttribute manuallyAddedAttribute : manuallyAddedAttributes) {
						WriterI attributeWriter = getTransformationTarget().getWriterInstance(manuallyAddedAttribute);
		
						if (attributeWriter != null) {
							if (counter > 0 && counter % numberOfAttributesPerLine == 0) {
								wNL();
							} else {
							    w(space);
							}
							
							attributeWriter.transform(ts);
							numberOfManuallyAddedAttributes++;
							space = " ";
							counter++;
						}
					}
				}
			}
		}
		
		if (counter > 0) outdent();
		
		return numberOfManuallyAddedAttributes;
	}
}


/*
 * Copyright (c) 2008 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.generation.xml.target;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetDocumentI;
import org.jenerateit.writer.WriterException;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.gs.gapp.generation.basic.GenerationPreventableByUserI;
import com.gs.gapp.metamodel.xml.XMLAttribute;
import com.gs.gapp.metamodel.xml.XMLElement;


/**
 * {@link TargetDocumentI} implementation for a XML target document.
 *
 * @author hrr
 */
public class XMLDocumentTargetDocument extends AbstractTextTargetDocument implements GenerationPreventableByUserI {

	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = 4671879045492165602L;
	/** information about the prolog section of an XML file */
	public static final TargetSection PROLOG = new TargetSection("prolog", 10);
	/** information about the content section of a XML file = the root element and its content */
	public static final TargetSection CONTENT = new TargetSection("content", 20);

	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<>(
			Arrays.asList(new TargetSection[] {
					PROLOG,
					CONTENT
			}));

	
	private boolean generationPreventedByUser = false;
	private StringBuilder documentContent;
	private Document previousDocument;
	
	private final Map<String,List<XMLAttribute>> attributes = new LinkedHashMap<>();
	private final Map<String,Map<String,XMLAttribute>> attributesMaps = new LinkedHashMap<>();
	private final Map<String,String> tagNames = new LinkedHashMap<>();
	
	private final Map<String,List<Element>> manuallAddedPredecessors = new LinkedHashMap<>();
	private final Map<String,List<Element>> manuallAddedSuccessors = new LinkedHashMap<>();
	private final Map<String,List<Element>> manuallAddedNestedElements = new LinkedHashMap<>();
	
	/**
	 * Default constructor.
	 */
	public XMLDocumentTargetDocument() {
		super();
	}

	/**
	 * Getter to request the XML comment end characters
	 *
	 * @return the comment end string
	 * @see TextTargetDocumentI#getCommentEnd()
	 */
	@Override
	public String getCommentEnd() {
		return "-->";
	}

	/**
	 * Getter to request the XML comment start characters
	 *
	 * @return the comment start string
	 * @see TextTargetDocumentI#getCommentStart()
	 */
	@Override
	public String getCommentStart() {
		return "<!--";
	}

	/**
	 * Returns all target sections this XML document supports
	 *
	 * @return the target sections of this XML target document implementation
	 * @see #PROLOG
	 * @see #CONTENT
	 * @see TextTargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

	@Override
	public char getPrefixChar() {
		return ' ';
	}
	
	@Override
	protected void analyzeDocument() {
		generationPreventedByUser = false;
		previousDocument = null;
		documentContent = new StringBuilder();
		super.analyzeDocument();
	}
	
	@Override
	protected void analyzeLine(int lineNumber, String line, int start, int end) {
		super.analyzeLine(lineNumber, line, start, end);

		documentContent.append(line);
		if (line != null && !generationPreventedByUser && line.contains("DO_NOT_GENERATE")) generationPreventedByUser = true;
	}

	@Override
	public boolean isGenerationPreventedByUser() {
		if (!isDocumentAnalyzed()) {
			analyzeDocument();
		}
		return generationPreventedByUser;
	}

	public Document getPreviousDocument() {
		if (!isDocumentAnalyzed()) {
			analyzeDocument();
		}
		createPreviousDocument();
		
		return previousDocument;
	}
	
	private void createPreviousDocument() {
		if (previousDocument == null && documentContent != null && documentContent.length() > 0) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			// see APPJSF-146 (mmt 2017-Sep-12)
			// source of information for this parameterization of the factory: https://stackoverflow.com/questions/155101/make-documentbuilder-parse-ignore-dtd-references
			factory.setValidating(false);
			factory.setNamespaceAware(true);
			try {
				factory.setFeature("http://xml.org/sax/features/namespaces", false);
				factory.setFeature("http://xml.org/sax/features/validation", false);
				factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
				factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			} catch (ParserConfigurationException ex) {
				ex.printStackTrace();
				throw new WriterException("error during configuration of XML parser:" + ex.getMessage(), ex);
			}
			
			DocumentBuilder builder;
			try {
				builder = factory.newDocumentBuilder();
				InputStream is = new ByteArrayInputStream(documentContent.toString().getBytes(StandardCharsets.UTF_8));
				previousDocument = builder.parse(is);
				readAttributesAndNodes();
			} catch (ParserConfigurationException | SAXException | IOException ex) {
				ex.printStackTrace();
				throw new WriterException("A problem occured while parsing existing XML document. The existing XML document must be valid, otherwise the generator won't generate the XML document since it cannot check the existing XML document for manual modifications. Please correct the XML document.", ex);
			}
		}
	}
	
	/**
	 * Fill the 'attributes' map by iterating through all elements of the document. 
	 */
	private void readAttributesAndNodes() {
		Objects.requireNonNull(previousDocument, "'previousDocument' is not yet available");
		
		NodeList nodeList = previousDocument.getElementsByTagName("*");
		for (int ii = 0; ii < nodeList.getLength(); ii++) {
	        Node node = nodeList.item(ii);
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	        	Element element = (Element) node;
	        	String id = getCombinedId(element);
	        	if (id == null) {
	        		id = element.getAttribute("id");
	        	}
	        	
	        	if (id == null || id.length() == 0) {
	        		continue;  // not sufficient context to apply the logic of preserving manual changes
	        	}
	        	
	        	// Keep the tag name in mind in order to be able to find out whether
	        	// that tag id remains the same but the tag name changes (normally due
	        	// to a model change).
	        	tagNames.put(id, element.getTagName());
	        	
	        	List<XMLAttribute> attributeList = this.attributes.get(id);
	        	if (attributeList == null) {
	        		attributeList = new ArrayList<>();
	        		this.attributes.put(id, attributeList);
	        	}
	        	Map<String,XMLAttribute> attributeMap = this.attributesMaps.get(id);
	        	if (attributeMap == null) {
	        		attributeMap = new HashMap<>();
	        		this.attributesMaps.put(id, attributeMap);
	        	}
	        	
	            NamedNodeMap attributes = element.getAttributes();
	            for (int jj = 0; jj < attributes.getLength(); jj++) {
	                Attr attr = (Attr) attributes.item(jj);
	                String attrName = attr.getNodeName();
	                String attrValue = attr.getNodeValue();
	                XMLAttribute xmlAttribute = new XMLAttribute(attrName, attrValue);
	                attributeList.add(xmlAttribute);
	                attributeMap.put(attrName, xmlAttribute);
	            }
	            
	            // --- process element in order to keep manually added nodes in mind
	            readElement(element);
	        }
	    }
	}
	
	/**
	 * @param element
	 * @return
	 */
	private String getCombinedId(Element element) {
		Node parentNode = element.getParentNode();
		Element parentElement = null;
		if (parentNode != null && parentNode.getNodeType() == Node.ELEMENT_NODE) {
			parentElement = (Element) parentNode;
			String elementId = element.getAttribute("id");
        	String parentElementId = parentElement.getAttribute("id");
        	if (elementId != null && elementId.length() > 0 && parentElementId != null && parentElementId.length() > 0) {
        		return elementId + "#" + parentElementId;
        	}
		}
    	
		return null;
	}
	
	private void readElement(Element element) {
		if (element.getNodeType() == Node.ELEMENT_NODE && !isManuallyAdded(element)) {
			Element generatedElement = null;
			Element previousGeneratedElement = null;
			List<Element> manuallyAddedElements = new ArrayList<>();
			int numberOfGeneratedElements = 0;
			for (int ii=0; ii<element.getChildNodes().getLength(); ii++) {
				Node childNode = element.getChildNodes().item(ii);
				if (childNode.getNodeType() != Node.ELEMENT_NODE) continue;
					
				Element childElement = (Element) childNode;
				if (isManuallyAdded(childElement)) {
					if (manuallyAddedElements == null) {
						manuallyAddedElements = new ArrayList<>();
						if (generatedElement != null) {
							this.manuallAddedSuccessors.put(getCombinedId(generatedElement), manuallyAddedElements);
						}
					}
					manuallyAddedElements.add(childElement);
				} else {
					numberOfGeneratedElements++;
					previousGeneratedElement = generatedElement;
					generatedElement = childElement;
					if (previousGeneratedElement == null && manuallyAddedElements.size() > 0) {
						// there is no generated element "before" the manually added ones (on the same level of depth in the XML tree)
						this.manuallAddedPredecessors.put(getCombinedId(generatedElement), manuallyAddedElements);
					}
					
					manuallyAddedElements = null;  // reset this to null in order to make a leading if-statement work fine
				}
			}
			
			// --- finally, check whether ALL children were manually added
			if (numberOfGeneratedElements == 0) {
				for (int ii=0; ii<element.getChildNodes().getLength(); ii++) {
					Node childNode = element.getChildNodes().item(ii);
					if (childNode.getNodeType() != Node.ELEMENT_NODE) continue;
					Element childElement = (Element) childNode;
					List<Element> nestedElements = this.manuallAddedNestedElements.computeIfAbsent(getCombinedId(element), key -> new ArrayList<>());
					nestedElements.add(childElement);
				}
			}
		}
	}
	
	/**
	 * @param node
	 * @return
	 */
	private boolean isManuallyAdded(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) node;
			String preserveAttribute = element.getAttribute("pt:data-vd-preserve");
			return preserveAttribute != null && preserveAttribute.length() > 0;
		}
		
		return false;
	}

	/**
	 * @return the attributes
	 */
	public Map<String,List<XMLAttribute>> getAttributes() {
		return attributes;
	}
	
	/**
	 * @param actualXmlElement
	 * @param excludedAttributes
	 * @return
	 */
	public List<XMLAttribute> getAttributes(XMLElement actualXmlElement, final Collection<String> excludedAttributes) {
		String id = actualXmlElement.getCombinedId();
		if (id == null) {
    		id = actualXmlElement.getEffectiveId();
    	}
		
		List<XMLAttribute> attributeList = attributes.get(id);
		
		if (attributeList == null || attributeList.isEmpty()) {
			return Collections.emptyList();
		} else if (actualXmlElement.getQualifiedName() != null && !actualXmlElement.getQualifiedName().equals(this.tagNames.get(id))) {
			// the tag name has changed compared to previous generation => we do not preserve any attributes at all (mmt 06-Apr-2020)
			return Collections.emptyList();
		}
		
		ArrayList<XMLAttribute> result = new ArrayList<>(attributeList);
		result.removeIf( (attrIn) -> excludedAttributes.contains(attrIn.getName()) );
		
        return result;
	}
	
	/**
	 * @param actualXmlElementId
	 * @param excludedAttributes
	 * @return
	 */
	public List<XMLAttribute> getAttributes(String actualXmlElementId, final Collection<String> excludedAttributes) {
		List<XMLAttribute> attributeList = attributes.get(actualXmlElementId);
		
		if (attributeList == null || attributeList.isEmpty()) {
			return Collections.emptyList();
		}
		
		ArrayList<XMLAttribute> result = new ArrayList<>(attributeList);
		result.removeIf( (attrIn) -> excludedAttributes.contains(attrIn.getName()) );
		
        return result;
	}

	/**
	 * @return the manuallAddedPredecessors
	 */
	public Map<String, List<Element>> getManuallAddedPredecessors() {
		return manuallAddedPredecessors;
	}

	/**
	 * @return the manuallAddedSuccessors
	 */
	public Map<String, List<Element>> getManuallAddedSuccessors() {
		return manuallAddedSuccessors;
	}

	/**
	 * @return the manuallAddedNestedElements
	 */
	public Map<String, List<Element>> getManuallAddedNestedElements() {
		return manuallAddedNestedElements;
	}

	/**
	 * @return the tagNames
	 */
	public Map<String,String> getTagNames() {
		return tagNames;
	}

	/**
	 * @return the attributesMaps
	 */
	public Map<String,Map<String,XMLAttribute>> getAttributesMaps() {
		return attributesMaps;
	}
}

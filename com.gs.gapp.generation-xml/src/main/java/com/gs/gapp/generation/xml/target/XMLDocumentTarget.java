package com.gs.gapp.generation.xml.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.xml.XMLDocument;
import com.gs.gapp.metamodel.xml.XMLElement;


public class XMLDocumentTarget extends BasicTextTarget<XMLDocumentTargetDocument> {

	@ModelElement
	private XMLDocument document;
	
	private XMLElement elementWithStartedDeveloperArea;
	
	@ContextObject
	private String fileNamePostfix;

	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/").append(getTargetPrefix()).append(document.getName()).append(".xml");
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, document);
		}
	}

	public XMLElement getElementWithStartedDeveloperArea() {
		return elementWithStartedDeveloperArea;
	}

	public void setElementWithStartedDeveloperArea(
			XMLElement elementWithStartedDeveloperArea) {
		this.elementWithStartedDeveloperArea = elementWithStartedDeveloperArea;
	}

	protected String getFileNamePostfix() {
		return document.getFilenamePostfix() != null && document.getFilenamePostfix().length() > 0 ?
				document.getFilenamePostfix() : fileNamePostfix;
	}
	
	/**
	 * @return true if the target file's name ends with _generated.[file-extension]
	 */
	public boolean isFilledWithGeneratedContent() {
		return (fileNamePostfix == null || fileNamePostfix.length() == 0) ?
				false : (getTargetURI().toString().indexOf(fileNamePostfix + ".") > 0);
	}
}

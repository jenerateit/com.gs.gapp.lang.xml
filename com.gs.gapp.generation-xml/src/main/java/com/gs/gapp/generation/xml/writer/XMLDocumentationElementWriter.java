package com.gs.gapp.generation.xml.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.xml.XMLDocumentationElement;

public class XMLDocumentationElementWriter extends XMLElementWriter {

	@ModelElement
	private XMLDocumentationElement element;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		writeElement(ts);
	}
	
	/**
	 * @param ts
	 */
	@Override
	protected void writeElement(TargetSection ts) {
		wNL();
		w("<!-- ");wValue(ts);wChildElements(ts);w(" -->");
	}

	@Override
	protected int wAttributes(TargetSection ts) {
		// nothing to be written for a documentation element
		return 0;
	}
}


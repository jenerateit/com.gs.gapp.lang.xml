package com.gs.gapp.generation.xml.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.metamodel.xml.XMLDoctype;
import com.gs.gapp.metamodel.xml.XMLProcessingInstruction;
import com.gs.gapp.metamodel.xml.XMLProlog;

public class XMLPrologWriter extends XmlWriter {

	@ModelElement
	private XMLProlog prolog;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {

		wProcessingInstructions(ts);
		wDoctypes(ts);
	}

	protected void wDoctypes(TargetSection ts) {
		for (XMLProcessingInstruction processingInstruction : prolog.getProcessingInstructions()) {
			WriterI processingInstructionWriter = getTransformationTarget().getWriterInstance(processingInstruction);
			if (processingInstructionWriter != null) {
				processingInstructionWriter.transform(ts);
				wNL();
			}
		}
	}

	protected void wProcessingInstructions(TargetSection ts) {
		for (XMLDoctype docType : prolog.getDoctypes()) {
			WriterI docTypeWriter = getTransformationTarget().getWriterInstance(docType);
			if (docTypeWriter != null) {
				docTypeWriter.transform(ts);
				wNL();
			}
		}
	}
}


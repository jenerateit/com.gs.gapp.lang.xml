/**
 *
 */
package com.gs.gapp.generation.xml;

import java.util.Set;

import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.xml.target.XMLDocumentTarget;
import com.gs.gapp.generation.xml.writer.XMLAttributeWriter;
import com.gs.gapp.generation.xml.writer.XMLDeveloperAreaElementWriter;
import com.gs.gapp.generation.xml.writer.XMLDoctypeWriter;
import com.gs.gapp.generation.xml.writer.XMLDocumentWriter;
import com.gs.gapp.generation.xml.writer.XMLDocumentationElementWriter;
import com.gs.gapp.generation.xml.writer.XMLElementWriter;
import com.gs.gapp.generation.xml.writer.XMLPlaceholderElementWriter;
import com.gs.gapp.generation.xml.writer.XMLProcessingInstructionWriter;
import com.gs.gapp.generation.xml.writer.XMLPrologWriter;
import com.gs.gapp.metamodel.xml.XMLAttribute;
import com.gs.gapp.metamodel.xml.XMLDeveloperAreaElement;
import com.gs.gapp.metamodel.xml.XMLDoctype;
import com.gs.gapp.metamodel.xml.XMLDocument;
import com.gs.gapp.metamodel.xml.XMLDocumentationElement;
import com.gs.gapp.metamodel.xml.XMLElement;
import com.gs.gapp.metamodel.xml.XMLPlaceholderElement;
import com.gs.gapp.metamodel.xml.XMLProcessingInstruction;
import com.gs.gapp.metamodel.xml.XMLProlog;



/**
 * @author mmt
 *
 */
public class WriterLocatorXML extends AbstractWriterLocator implements WriterLocatorI {

	public WriterLocatorXML(Set<Class<? extends TargetI<?>>> targetClasses) {
		super(targetClasses);

		// --- mappers for generation decision
	    addWriterMapperForGenerationDecision( new WriterMapper(XMLDocument.class, XMLDocumentTarget.class, XMLDocumentWriter.class) );

	    // --- mappers for writer delegation
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLAttribute.class, XMLDocumentTarget.class, XMLAttributeWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLProlog.class, XMLDocumentTarget.class, XMLPrologWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLDoctype.class, XMLDocumentTarget.class, XMLDoctypeWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLProcessingInstruction.class, XMLDocumentTarget.class, XMLProcessingInstructionWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLElement.class, XMLDocumentTarget.class, XMLElementWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLPlaceholderElement.class, XMLDocumentTarget.class, XMLPlaceholderElementWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLDocumentationElement.class, XMLDocumentTarget.class, XMLDocumentationElementWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(XMLDeveloperAreaElement.class, XMLDocumentTarget.class, XMLDeveloperAreaElementWriter.class) );
	    
	    
	}
}

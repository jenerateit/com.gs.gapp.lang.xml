package com.gs.gapp.generation.xml.writer;

import java.util.Map;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.xml.target.XMLDocumentTargetDocument;
import com.gs.gapp.metamodel.xml.XMLAttribute;
import com.gs.gapp.metamodel.xml.XMLElement;

public class XMLAttributeWriter extends XmlWriter {

	@ModelElement
	private XMLAttribute attribute;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		// value of an attribute can be an empty string (https://stackoverflow.com/questions/184014/can-an-xml-attribute-be-the-empty-string)
		if ("id".equalsIgnoreCase(attribute.getName()) && attribute.getOwner() != null) {
			if (attribute.getOwner().getEffectiveId() != null) {
				w(attribute.getQualifiedName(), "=", "\"", attribute.getOwner().getEffectiveId(), "\"");
			}
		} else {
			String effectiveAttributeValue = getEffectiveAttributeValue(attribute);
            w(attribute.getQualifiedName(), "=", "\"", effectiveAttributeValue == null ? "" : effectiveAttributeValue, "\"");
		}
	}
	
	/**
	 * @param attribute
	 * @return
	 */
	protected String getEffectiveAttributeValue(XMLAttribute attribute) {
	    String value = attribute.getValue();
	    
	    Map<String, String> patterns = getGenerationGroupOptions().getAttributePreservationPatterns();
	    
	    if ((!patterns.isEmpty()) && patterns.keySet().contains(attribute.getQualifiedName())) {
	    	XMLDocumentTargetDocument previousDocument =
					(XMLDocumentTargetDocument) this.getTextTransformationTarget().getPreviousTargetDocument();
	    	XMLElement owningElement = attribute.getOwner();
	    	String id = owningElement.getCombinedId();
			if (id == null) {
	    		id = owningElement.getIdAttribute();
	    	}
	    	Map<String, XMLAttribute> attributesMap = previousDocument.getAttributesMaps().get(id);
	    	if (attributesMap != null) {
	    		XMLAttribute previousAttribute = attributesMap.get(attribute.getQualifiedName());
	    		String pattern = patterns.get(attribute.getQualifiedName());
	    		if (previousAttribute != null && previousAttribute.getValue() != null) {
	    			
	    			if (pattern == null || pattern.length() == 0) { // just replace the value
	    				if (!previousAttribute.getValue().equals(value)) {
	    					getTransformationTarget().addInfo("attribute '" + attribute.getQualifiedName() +
	    							"' of XML tag '" + owningElement.getQualifiedName() + "' has manually overwritten value '" + previousAttribute.getValue() +
	    							"' (generated would be '" + value + "')"); 
	    				}
	    				value = previousAttribute.getValue();
	    			} else if (attribute.getValue().matches(pattern)) { // only replace the value when the given pattern matches
	    				if (!previousAttribute.getValue().equals(value)) {
	    					getTransformationTarget().addInfo("attribute '" + attribute.getQualifiedName() +
	    							"' of XML tag '" + owningElement.getQualifiedName() + "' has manually overwritten value '" + previousAttribute.getValue() +
	    							"' (generated would be '" + value + "')"); 
	    				}
	    				value = previousAttribute.getValue();
	    			}
	    		}
	    	}
	    }
	    
		return value;
	}
}


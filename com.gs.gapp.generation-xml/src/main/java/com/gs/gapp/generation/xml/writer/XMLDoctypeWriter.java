package com.gs.gapp.generation.xml.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.xml.XMLDoctype;

public class XMLDoctypeWriter extends XmlWriter {

	@ModelElement
	private XMLDoctype doctype;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
        w(doctype.getDoctype().toLowerCase().contains("!doctype") ? "<" : "<!DOCTYPE ", doctype.getDoctype(), ">");
	}
}


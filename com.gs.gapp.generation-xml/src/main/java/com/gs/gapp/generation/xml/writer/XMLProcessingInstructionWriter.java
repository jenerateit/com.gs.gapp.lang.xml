package com.gs.gapp.generation.xml.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.xml.XMLProcessingInstruction;

public class XMLProcessingInstructionWriter extends XmlWriter {

	@ModelElement
	private XMLProcessingInstruction processingInstruction;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
        w("<?", processingInstruction.getProcessingInstruction(), " ?>");
	}
}


package com.gs.gapp.generation.xml.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.xml.XMLDeveloperAreaElement;

public class XMLDeveloperAreaElementWriter extends XMLElementWriter {

	@ModelElement
	private XMLDeveloperAreaElement element;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.xml.writer.XMLElementWriter#onBeginDeveloperArea(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected boolean onBeginDeveloperArea(TargetSection ts) {
		bDA(element.getName());
		return true;
	}
	
	/**
	 * @param ts
	 */
	@Override
	protected void writeElement(TargetSection ts) {
		// nothing to be written for a developer area element
	}

	@Override
	protected int wAttributes(TargetSection ts) {
		// nothing to be written for a developer area element
		return 0;
	}
}


/*
 * Copyright (c) 2008 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.generation.xml.writer;

import java.util.Iterator;
import java.util.Properties;

import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.generation.xml.GenerationGroupXML.GenerationGroupXmlOptions;
import com.gs.gapp.generation.xml.target.XmlTarget;
import com.gs.gapp.generation.xml.target.XmlTargetDocument;

/**
 * The XML writer enhances the {@link AbstractTextWriter} class 
 * with some useful methods writing XML files.
 * 
 * @author hrr
 */
public abstract class XmlWriter extends ModelElementWriter {
	
	private GenerationGroupXmlOptions generationGroupXmlOptions;

	/**
	 * inner class for XML attributes
	 */
	protected class XmlAttribute {
		private final String key;
		private final String value;
		
		/**
		 * Constructor
		 * 
		 * @param key the key of the attribute
		 * @param value the value of the attribute
		 */
		public XmlAttribute(String key, String value) {
			if (!StringTools.isText(key)) {
				throw new IllegalArgumentException("The key value '" + key + "' is not valid");
			}
			this.key = key;
			this.value = value != null ? value : StringTools.EMPTY_STRING;
		}
	}
	
	/**
	 * Write a header comment to the new XML target document
	 * <b>Note:</b> The '&lt;?' will be added in front of the line and the '?&gt;' at the end. 
	 * If the line has line separators each line will be added separately.
	 * 
	 * @param comments the header comment(s) to add
	 * @return an instance of this writer object
	 */
	public XmlWriter wHeader(String... comments) {
		if (comments == null || comments.length == 0) {
			
		} else {
			checkTarget();
			for (String l : comments) {				
				for (CharSequence s : getLines(l)) {
					wNL(XmlTargetDocument.PROLOG, "<?" + s + "?>");					
				}
			}
		}
		return this;
	}
	

	/**
	 * Writes a complete XML element to the target document.
	 * <pre>
	 * <code>
	 * Properties p = new Properties();
	 * p.put("firstname", "August");
	 * p.put("lastname", "Habenichts");
	 * wElement("person", p);
	 * </code>
	 * </pre> 
	 * Will result in an XML tag like this:
	 * <pre>
	 * <code>
	 * &lt;person firstname="Anton" lastname="Habenichts" /&gt;
	 * </code>
	 * </pre>
	 * 
	 * @param name the name of the element to write
	 * @param attributes the attributes to add into the element (null if none)
	 * @return an instance of this writer object
	 */
	public XmlWriter wElement(String name, Properties attributes) {
		if (!StringTools.isText(name)) {
			throw new WriterException("The element name to write is not valid", getTransformationTarget(), this);
			
		} else {
			checkTarget();
			if (attributes != null && !attributes.isEmpty()) {
				wNLI(XmlTargetDocument.CONTENT, "<", name);
				Iterator<Object> i = attributes.keySet().iterator();
				while (i.hasNext()) {
					final Object k = i.next();
					w(XmlTargetDocument.CONTENT, k.toString(), "=", getQuotedString(attributes.getProperty(k.toString(), StringTools.EMPTY_STRING)));						
					if (i.hasNext()) {
						wNL();
					}
				}
				wNLO(XmlTargetDocument.CONTENT, "/>");
			} else {
				wNL(XmlTargetDocument.CONTENT, "<", name, "/>");
				
			}
		}
		return this;
	}

	/**
	 * Writes a complete XML element to the target document.
	 * <pre>
	 * <code>
	 * wElement("person", new XmlAttribute("firstname", "August"), new XmlAttribute("lastname", "Habenichts"));
	 * </code>
	 * </pre> 
	 * Will result in an XML tag like this:
	 * <pre>
	 * <code>
	 * &lt;person firstname="Anton" lastname="Habenichts" /&gt;
	 * </code>
	 * </pre>
	 * 
	 * @param name the name of the element to write
	 * @param attributes the attributes to add into the element (null if none)
	 * @return an instance of this writer object
	 */
	public XmlWriter wElement(String name, XmlAttribute... attributes) {
		if (!StringTools.isText(name)) {
			throw new WriterException("The element name to write is not valid", getTransformationTarget(), this);
			
		} else {
			checkTarget();
			if (attributes != null && attributes.length > 0) {
				wNLI(XmlTargetDocument.CONTENT, "<", name);
				for (int i = 0; i < attributes.length; i++) {
					if (i == attributes.length - 1) {
						w(XmlTargetDocument.CONTENT, " ", attributes[i].key, "=", getQuotedString(attributes[i].value));
					} else {
						wNL(XmlTargetDocument.CONTENT, " ", attributes[i].key, "=", getQuotedString(attributes[i].value));
					}
				}
				wNLO(XmlTargetDocument.CONTENT, "/>");
			} else {
				wNL(XmlTargetDocument.CONTENT, "<", name, "/>");
			}
		}
		return this;
	}

	/**
	 * Writes a start XML element to the target document.
	 * <pre>
	 * <code>
	 * Properties p = new Properties();
	 * p.put("firstname", "August");
	 * p.put("lastname", "Habenichts");
	 * wStartElement("person", p);
	 * </code> 
	 * </pre>
	 * Will result in a XML tag like this:
	 * <pre>
	 * <code>
	 * &lt;person firstname="Anton" lastname="Habenichts" &gt;
	 * </code>
	 * </pre>
	 * 
	 * @param name the name of the element to write
	 * @param attributes the attributes to add into the element (null if none)
	 * @return an instance of this writer object
	 */
	public XmlWriter wStartElement(String name, Properties attributes) {
		if (!StringTools.isText(name)) {
			throw new WriterException("The element name to write is not valid", getTransformationTarget(), this);
			
		} else {
			checkTarget();
			if (attributes != null && !attributes.isEmpty()) {
				wNLI(XmlTargetDocument.CONTENT, "<", name);
				Iterator<Object> i = attributes.keySet().iterator();
				while (i.hasNext()) {
					final Object k = i.next();
					w(XmlTargetDocument.CONTENT, k.toString(), "=", getQuotedString(attributes.getProperty(k.toString(), StringTools.EMPTY_STRING)));						
					if (i.hasNext()) {
						wNL();
					}
				}
				wNL(XmlTargetDocument.CONTENT, ">");
			} else {
				wNLI(XmlTargetDocument.CONTENT, "<", name, ">");
				
			}
		}
		return this;
	}

	/**
	 * Writes a start XML element to the target document.
	 * <pre>
	 * <code>
	 * wStartElement("person");
	 * </code> 
	 * </pre>
	 * Will result in a XML tag like this:
	 * <pre>
	 * <code>
	 * &lt;person&gt;
	 * </code>
	 * </pre>
	 * 
	 * @param name the name of the element to write
	 * @return an instance of this writer object
	 */
	public XmlWriter wStartElement(String name) {
		if (!StringTools.isText(name)) {
			throw new WriterException("The element name to write is not valid", getTransformationTarget(), this);
			
		} else {
			checkTarget();
			wNLI(XmlTargetDocument.CONTENT, "<", name, ">");
		}
		return this;
	}

	/**
	 * Writes a start XML element to the target document.
	 * <pre>
	 * <code>
	 * wStartElement("person", new XmlAttribute("firstname", "August"), new XmlAttribute("lastname", "Habenichts"));
	 * </code> 
	 * </pre>
	 * Will result in a XML tag like this:
	 * <pre>
	 * <code>
	 * &lt;person firstname="Anton" lastname="Habenichts" &gt;
	 * </code>
	 * </pre>
	 * 
	 * @param name the name of the element to write
	 * @param attributes the attributes to add into the element (null if none)
	 * @return an instance of this writer object
	 */
	public XmlWriter wStartElement(String name, XmlAttribute... attributes) {
		if (!StringTools.isText(name)) {
			throw new WriterException("The element name to write is not valid", getTransformationTarget(), this);
			
		} else {
			checkTarget();
			if (attributes != null && attributes.length > 0) {
				wNLI(XmlTargetDocument.CONTENT, "<", name);
				for (int i = 0; i < attributes.length; i++) {
					if (i == attributes.length - 1) {
						w(XmlTargetDocument.CONTENT, " ", attributes[i].key, "=", getQuotedString(attributes[i].value));
					} else {
						wNL(XmlTargetDocument.CONTENT, " ", attributes[i].key, "=", getQuotedString(attributes[i].value));
					}
				}
				wNL(XmlTargetDocument.CONTENT, ">");
			} else {
				wNLI(XmlTargetDocument.CONTENT, "<", name, ">");
			}
		}
		return this;
	}

	/**
	 * Writes an end XML element to the target document.
	 * <pre>
	 * <code>
	 * wEndElement("person");
	 * </code>
	 * </pre> 
	 * Will result in an XML tag like this:
	 * <pre>
	 * <code>
	 * &lt;/person&gt;
	 * </code>
	 * </pre>
	 * @param name the name of the element to write
	 * @return an instance of this writer object
	 */
	public XmlWriter wEndElement(String name) {
		if (!StringTools.isText(name)) {
			throw new WriterException("The element name to write is not valid", getTransformationTarget(), this);
			
		} else {
			checkTarget();
			wONL(XmlTargetDocument.CONTENT, "</", name, ">");

		}
		return this;
	}
	
	/**
	 * Method to check if the target file in the transformation context is of type {@link XmlTarget}
	 */
	private void checkTarget() {
		if (!(getTransformationTarget() instanceof XmlTarget)) {
			throw new WriterException("The target '" + getTransformationTarget().getClass().getName() + 
					"' in the TransformationContext is not of type '" + 
					XmlTarget.class.getName() + "'", getTransformationTarget(), this);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.writer.ModelElementWriter#getGenerationGroupOptions()
	 */
	@Override
	public GenerationGroupXmlOptions getGenerationGroupOptions() {
		if (this.generationGroupXmlOptions == null) {
			this.generationGroupXmlOptions = new GenerationGroupXmlOptions(this);
		}
		
		return this.generationGroupXmlOptions;
	}
}

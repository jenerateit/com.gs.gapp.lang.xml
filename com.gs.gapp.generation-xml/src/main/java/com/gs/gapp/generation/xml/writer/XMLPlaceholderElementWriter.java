package com.gs.gapp.generation.xml.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.xml.XMLPlaceholderElement;

public class XMLPlaceholderElementWriter extends XMLElementWriter {

	@ModelElement
	private XMLPlaceholderElement element;

	/**
	 * Override this method when you want to write individual content instead of the content
	 * that is originally represented by the XML element. 
	 * 
	 * @param ts
	 */
	@Override
	protected void writeElement(TargetSection ts) {
		if (element.getContent() == null) {
		    super.writeElement(ts);
		} else {
			wNL();
			w(element.getContent());
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.xml.writer.XMLElementWriter#onBeginDeveloperArea(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected boolean onBeginDeveloperArea(TargetSection ts) {
		bDA(element.getQualifiedId());
		return true;
	}
}

